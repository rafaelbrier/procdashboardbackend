package com.procdashboard.Application.Entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

@Entity
@Table(name="PROCEDIMENTOS", catalog = "monit")
public class ProcAvailable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO )
	@Column(name = "procId")
	private Integer procId;
	
	@JsonIgnore
	@OneToMany(mappedBy = "procAvailable", targetEntity = ProcLogExec.class, cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
	private List<ProcLogExec> procLogExec;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "malhaId")
	private Malha malha;
	
	@NotEmpty
	@Column(name = "nome")
	private String nome;
	
	@NotEmpty
	@Column(name = "banco")
	private String banco;
	
	@NotEmpty
	@Lob 	
	@Column(name = "descricao", columnDefinition = "text")
	private String descricao;
	
	@NotEmpty
	@Lob 	
	@Column(name = "observacao", columnDefinition = "text")
	private String observacao;
	
	@NotEmpty
	@Lob 	
	@Column(name = "erroObservacao", columnDefinition = "text")
	private String erroObservacao;
	
	@Transient
	private String malhaNome;
	
	@NotEmpty
	@Column(name = "criticidade")
	private String criticidade;
	
	@NotEmpty
	@Column(name = "fechamento")
	private String fechamento;
	
	@NotEmpty
	@Column(name = "sistema")
	private String sistema;
	
	@NotEmpty
	@Column(name = "jobNome")
	private String jobNome;
	
	@NotEmpty
	@Column(name = "periodicidade")
	private String periodicidade;
	
	@Column(name = "horaInicio")
	private String horaInicio;
	
	@Column(name = "horaFim")
	private String horaFim;
	
	@Column(name = "intervaloExec")
	private String intervaloExec;
	
	@Column(name = "diaInicio")
	private String diaInicio;
	
	@Column(name = "diaFim")
	private String diaFim;
	
	@DateTimeFormat(pattern = "HH:mm:ss")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@Column(name = "tempoEstimado")
	private LocalDateTime tempoEstimado;
	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@Column(name = "ultimaExecucao")
	private LocalDateTime ultimaExecucao;
	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@Column(name = "criadoEm")
	private LocalDateTime criadoEm;
	
	@Transient
	private Integer tempoMedio;
	
	@PrePersist
	public void setupDate() {
		this.criadoEm = LocalDateTime.now();
	}

	public Integer getProcId() {
		return procId;
	}

	public void setProcId(Integer procId) {
		this.procId = procId;
	}

	public List<ProcLogExec> getProcLogExec() {
		return procLogExec;
	}

	public void setProcLogExec(List<ProcLogExec> procLogExec) {
		this.procLogExec = procLogExec;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public String getErroObservacao() {
		return erroObservacao;
	}

	public void setErroObservacao(String erroObservacao) {
		this.erroObservacao = erroObservacao;
	}

	public Malha getMalha() {
		return malha;
	}

	public void setMalha(Malha malha) {
		this.malha = malha;
	}
	
	public String getMalhaNome() {
		malhaNome = this.malha.getNome();
		return malhaNome;
	}

	public void setMalhaNome(String malhaNome) {
		this.malhaNome = malhaNome;
	}

	public String getCriticidade() {
		return criticidade;
	}

	public void setCriticidade(String criticidade) {
		this.criticidade = criticidade;
	}

	public String getFechamento() {
		return fechamento;
	}

	public void setFechamento(String fechamento) {
		this.fechamento = fechamento;
	}

	public String getSistema() {
		return sistema;
	}

	public void setSistema(String sistema) {
		this.sistema = sistema;
	}

	public String getPeriodicidade() {
		return periodicidade;
	}

	public void setPeriodicidade(String periodicidade) {
		this.periodicidade = periodicidade;
	}

	public String getJobNome() {
		return jobNome;
	}

	public void setJobNome(String jobNome) {
		this.jobNome = jobNome;
	}

	public String getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	
	public String getHoraFim() {
		return horaFim;
	}

	public void setHoraFim(String horaFim) {
		this.horaFim = horaFim;
	}

	public String getIntervaloExec() {
		return intervaloExec;
	}

	public void setIntervaloExec(String intervaloExec) {
		this.intervaloExec = intervaloExec;
	}

	public String getDiaFim() {
		return diaFim;
	}

	public void setDiaFim(String diaFim) {
		this.diaFim = diaFim;
	}

	public LocalDateTime getTempoEstimado() {
		return tempoEstimado;
	}

	public void setTempoEstimado(LocalDateTime tempoEstimado) {
		this.tempoEstimado = tempoEstimado;
	}

	public LocalDateTime getUltimaExecucao() {
		return ultimaExecucao;
	}

	public void setUltimaExecucao(LocalDateTime ultimaExecucao) {
		this.ultimaExecucao = ultimaExecucao;
	}

	public LocalDateTime getCriadoEm() {
		return criadoEm;
	}

	public void setCriadoEm(LocalDateTime criadoEm) {
		this.criadoEm = criadoEm;
	}

	public String getDiaInicio() {
		return diaInicio;
	}

	public void setDiaInicio(String diaInicio) {
		this.diaInicio = diaInicio;
	}

	public Integer getTempoMedio() {
		return tempoMedio;
	}

	public void setTempoMedio(Integer tempoMedio) {
		this.tempoMedio = tempoMedio;
	}
	
}
