package com.procdashboard.Application.Entity;

import java.time.LocalDate;

public interface ProcChartData {

	public Integer getTempoExecucao();	
	public LocalDate getDataInicio();
    public Integer getNumeroErros();
    public Integer getNumeroExecucao();
	
}
