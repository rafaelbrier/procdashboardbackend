package com.procdashboard.Application.Entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="MALHAS", catalog = "monit")
public class Malha {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "malhaId")
	private Integer malhaId;
	
	@Column(name = "nome")
	private String nome;
	
	@NotEmpty
	@Lob 	
	@Column(name = "descricao", columnDefinition = "text")
	private String descricao;
	
	@JsonIgnore
	@OneToMany(mappedBy = "malha", targetEntity = ProcAvailable.class, cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
	private List<ProcAvailable> procAvailable;

	public Integer getMalhaId() {
		return malhaId;
	}

	public void setMalhaId(Integer malhaId) {
		this.malhaId = malhaId;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<ProcAvailable> getProcAvailable() {
		return procAvailable;
	}

	public void setProcAvailable(List<ProcAvailable> procAvailable) {
		this.procAvailable = procAvailable;
	}
	
}
