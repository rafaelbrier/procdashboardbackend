package com.procdashboard.Application.Entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

@Entity
@Table(name="LOG_ERROR", catalog = "monit")
public class ProcLogError {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "logErrorId")
	private Integer logErrorId;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "logExecId")
	private ProcLogExec procLogExec;
	
	@Column(name = "logExecId", insertable = false, updatable = false)
	private Integer logExecId;
		
	@NotEmpty
	@Lob 	
	@Column(name = "mensagem", columnDefinition = "text")
	private String mensagem;
	
	@NotEmpty
	@Column(name = "codigo")
	private String codigo;
	
	@NotEmpty
	@Column(name = "critico")
	private String critico;
	
	@NotEmpty
	@Column(name = "linha")
	private String linha;
		
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@Column(name = "criadoEm")
	private LocalDateTime criadoEm;
	
	@PrePersist
	public void setupDate() {
		this.criadoEm = LocalDateTime.now();
	}

	public Integer getLogErrorId() {
		return logErrorId;
	}

	public void setLogErrorId(Integer logErrorId) {
		this.logErrorId = logErrorId;
	}

	public ProcLogExec getProcLogExec() {
		return procLogExec;
	}

	public void setProcLogExec(ProcLogExec procLogExec) {
		this.procLogExec = procLogExec;
	}

	public Integer getLogExecId() {
		return logExecId;
	}

	public void setLogExecId(Integer logExecId) {
		this.logExecId = logExecId;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getLinha() {
		return linha;
	}

	public void setLinha(String linha) {
		this.linha = linha;
	}

	public LocalDateTime getCriadoEm() {
		return criadoEm;
	}

	public void setCriadoEm(LocalDateTime criadoEm) {
		this.criadoEm = criadoEm;
	}

	public String getCritico() {
		return critico;
	}

	public void setCritico(String critico) {
		this.critico = critico;
	}
}
