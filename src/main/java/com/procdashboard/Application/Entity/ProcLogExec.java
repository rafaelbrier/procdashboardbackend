package com.procdashboard.Application.Entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

@Entity
@Table(name="LOG_EXEC", catalog = "monit")
public class ProcLogExec {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "logExecId")
	private Integer logExecId;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "procId")
	private ProcAvailable procAvailable;
			
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@Column(name = "dataInicio")
	private LocalDateTime dataInicio;
	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@Column(name = "dataFim")
	private LocalDateTime dataFim;
	
	@Column(name = "procId", insertable = false, updatable = false)
	private Integer procId;
	
	@Column(name = "tempoExecucao")
	private Integer tempoExecucao;
	
	@Transient
	private Integer tempoMedio;
		
	@NotEmpty
	@Column(name = "status")
	private String status;

	public Integer getLogExecId() {
		return logExecId;
	}

	public void setLogExecId(Integer logExecId) {
		this.logExecId = logExecId;
	}

	public ProcAvailable getProcAvailable() {
		return procAvailable;
	}

	public void setProcAvailable(ProcAvailable procAvailable) {
		this.procAvailable = procAvailable;
	}

	public LocalDateTime getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDateTime dataInicio) {
		this.dataInicio = dataInicio;
	}

	public LocalDateTime getDataFim() {
		return dataFim;
	}

	public void setDataFim(LocalDateTime dataFim) {
		this.dataFim = dataFim;
	}

	public Integer getTempoExecucao() {
		return tempoExecucao;
	}

	public void setTempoExecucao(Integer tempoExecucao) {
		this.tempoExecucao = tempoExecucao;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getProcId() {
		return procId;
	}

	public void setProcId(Integer procId) {
		this.procId = procId;
	}

	public Integer getTempoMedio() {
		return tempoMedio;
	}

	public void setTempoMedio(Integer tempoMedio) {
		this.tempoMedio = tempoMedio;
	}
	
}
