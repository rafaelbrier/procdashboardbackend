package com.procdashboard.Application.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.procdashboard.Application.Dao.MalhaDao;
import com.procdashboard.Application.Entity.Malha;

@Service
public class MalhaService {
	
	@Autowired
	MalhaDao malhaDao;
	
	public List<String> findMalhasNome() {
		return this.malhaDao.findMalhasNome();
	}
	
	public List<Malha> findMalhas() {
		return this.malhaDao.findMalhas();
	}

}
