package com.procdashboard.Application.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.procdashboard.Application.Dao.ProcAvailableDao;
import com.procdashboard.Application.Dao.ProcLogExecDao;
import com.procdashboard.Application.Entity.ProcAvailable;

@Service
public class ProcAvailableService {
	
	@Autowired
	ProcAvailableDao procAvailableDao;
	
	@Autowired
	ProcLogExecDao procLogExecDao;
		
	public List<ProcAvailable> findAllPageable(String nameSearch,
			String malhaSearch,
			String sistemaSearch) {
		
		List<ProcAvailable> procAvailableList = this.procAvailableDao.findAllPageable(nameSearch,malhaSearch,sistemaSearch);
		for(ProcAvailable procAvailable: procAvailableList) {
			procAvailable.setTempoMedio(this.procLogExecDao.findAverageTime(procAvailable.getProcId()));
		}
		
		return procAvailableList;
	}
	
	public Iterable<ProcAvailable> findAll() {
		return this.procAvailableDao.findAll();
	}
	
	public List<String> findSistemas() {
		return this.procAvailableDao.findSistemas();	
	}
	
}
