package com.procdashboard.Application.Service;

import java.util.List;
import java.util.Stack;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.procdashboard.Application.Dao.ProcLogExecDao;
import com.procdashboard.Application.Entity.ProcChartData;
import com.procdashboard.Application.Entity.ProcLogExec;

@Service
public class ProcLogExecService {
	
	@Autowired
	ProcLogExecDao procLogExecDao;
	
	public Page<ProcLogExec> findAll(String nameSearch,
			String startDateSearch,
			String malhaSearch,
			String jobSearch,
			String sistemaSearch,
			String execStatusSearch,
			Pageable pageable) {
		
		Page<ProcLogExec> procLogExecPage;
		procLogExecPage = this.procLogExecDao.findAll(
				nameSearch,
				startDateSearch, 
				malhaSearch,
				jobSearch,
				sistemaSearch,
				execStatusSearch,
				pageable);
		
		List<ProcLogExec> procLogExecContent;		
		procLogExecContent = procLogExecPage.getContent();
		
		Stack<Integer> procIdStack = new Stack<Integer>(); 
		Stack<Integer> tempoMedioStack = new Stack<Integer>(); 
		
		for(ProcLogExec procLogExec: procLogExecContent) {
			if(!procIdStack.contains(procLogExec.getProcId())) {
				procIdStack.push(procLogExec.getProcId());
			}
			procLogExec.setTempoMedio(procIdStack.indexOf(procLogExec.getProcId()));
		}
		
		for(Integer procIds : procIdStack)
		{
		    tempoMedioStack.push(this.procLogExecDao.findAverageTime(procIds));
		}
		
		for(ProcLogExec procLogExec: procLogExecContent) {
			procLogExec.setTempoMedio(tempoMedioStack.get(procLogExec.getTempoMedio()));
		}
		
		return procLogExecPage;
	}
	
	public ProcLogExec findById(Integer id) {
		return this.procLogExecDao.findById(id).orElse(null);
	}
	
	public List<ProcChartData> findChartData(Integer procId) {
		return this.procLogExecDao.findChartData(procId);
	}
	
	public ProcLogExec save(ProcLogExec news) {
		return this.procLogExecDao.save(news);
	}
	
	public void delete(Integer id) {
		this.procLogExecDao.deleteById(id);
	}
	
	
}
