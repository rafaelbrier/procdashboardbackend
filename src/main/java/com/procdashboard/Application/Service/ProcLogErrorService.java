package com.procdashboard.Application.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.procdashboard.Application.Dao.ProcLogErrorDao;
import com.procdashboard.Application.Entity.ProcLogError;

@Service
public class ProcLogErrorService {
	
	@Autowired 
	ProcLogErrorDao procLogErrorDao;
	
	public List<ProcLogError> findByLogExecId(Integer id) {
		return this.procLogErrorDao.findByLogExecId(id);
	}
}
