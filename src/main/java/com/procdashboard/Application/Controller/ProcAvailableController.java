package com.procdashboard.Application.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.procdashboard.Application.Entity.ProcAvailable;
import com.procdashboard.Application.Service.ProcAvailableService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/procavailable")
public class ProcAvailableController {
	
	@Autowired
	ProcAvailableService procAvailableService;
	
	private String formatForLikeQuery(String str) {
		if(str != null) {
			return "%".concat(str).concat("%");
		}
		return null;
	}
	
	@GetMapping()
	public List<ProcAvailable> findAllPageable(
			@RequestParam(value="nameSearch", required=false) String nameSearch,
			@RequestParam(value="malhaSearch", required=false) String malhaSearch,
			@RequestParam(value="sistemaSearch", required=false) String sistemaSearch) {
		nameSearch = formatForLikeQuery(nameSearch);
		malhaSearch = formatForLikeQuery(malhaSearch);
		sistemaSearch = formatForLikeQuery(sistemaSearch);
		return this.procAvailableService.findAllPageable(nameSearch, malhaSearch, sistemaSearch);
	}
	
	@GetMapping("/all")
	public Iterable<ProcAvailable> findAll() {
		return this.procAvailableService.findAll();
	}
		
	@GetMapping(path = "/sistemas")
	public List<String> findSistemas() {
		return this.procAvailableService.findSistemas();
	}
	
}
