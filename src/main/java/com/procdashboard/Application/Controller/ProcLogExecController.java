package com.procdashboard.Application.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.procdashboard.Application.Entity.ProcChartData;
import com.procdashboard.Application.Entity.ProcLogError;
import com.procdashboard.Application.Entity.ProcLogExec;
import com.procdashboard.Application.Service.ProcLogErrorService;
import com.procdashboard.Application.Service.ProcLogExecService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/proclogexec")
public class ProcLogExecController {
	
	@Autowired
	ProcLogExecService procLogExecService;
	
	@Autowired
	ProcLogErrorService procLogErrorService;
	
	@GetMapping()
	public Page<ProcLogExec> findAll(
			@RequestParam(value="nameSearch", required=false) String nameSearch,
			@RequestParam(value="startDateSearch", required=false) String startDateSearch,
			@RequestParam(value="malhaSearch", required=false) String malhaSearch,
			@RequestParam(value="jobSearch", required=false) String jobSearch,
			@RequestParam(value="sistemaSearch", required=false) String sistemaSearch,
			@RequestParam(value="execStatusSearch", required=false) String execStatusSearch,
			Pageable pageable) {
		nameSearch = formatForLikeQuery(nameSearch);
		startDateSearch = formatForLikeQuery(startDateSearch);
		malhaSearch = formatForLikeQuery(malhaSearch);
		jobSearch = formatForLikeQuery(jobSearch);
		sistemaSearch = formatForLikeQuery(sistemaSearch);
		return this.procLogExecService.findAll(nameSearch, startDateSearch, malhaSearch, jobSearch, sistemaSearch, execStatusSearch, pageable);
	}
			
	@GetMapping(path = "/{id}")
	public ProcLogExec findOne(@PathVariable Integer id) {
		return this.procLogExecService.findById(id);
	}
	
	@GetMapping(path = "/{id}/errors")
	public List<ProcLogError> findErrorsOfLogExec(@PathVariable Integer id) {
		return this.procLogErrorService.findByLogExecId(id);
	}
	
	@GetMapping(path = "/chart")
	public List<ProcChartData> findTemposExecucao(@RequestParam(value="procId") Integer procId) {
		return this.procLogExecService.findChartData(procId);
	}

	private String formatForLikeQuery(String str) {
		if(str != null) {
			return "%".concat(str).concat("%");
		}
		return null;
	}
}