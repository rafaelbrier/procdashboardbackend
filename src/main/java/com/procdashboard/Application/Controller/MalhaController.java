package com.procdashboard.Application.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.procdashboard.Application.Entity.Malha;
import com.procdashboard.Application.Service.MalhaService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/malhas")
public class MalhaController {
	
	@Autowired
	MalhaService malhaService;
	
	@GetMapping()
	public List<Malha> findMalhas() {
		return this.malhaService.findMalhas();
	}
	
	@GetMapping(path = "/nome")
	public List<String> findMalhasNome() {
		return this.malhaService.findMalhasNome();
	}

}
