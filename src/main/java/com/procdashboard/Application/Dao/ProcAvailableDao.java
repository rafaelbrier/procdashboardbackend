package com.procdashboard.Application.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.procdashboard.Application.Entity.ProcAvailable;

public interface ProcAvailableDao extends CrudRepository <ProcAvailable, Integer> {
	
	@Query(value = "SELECT * FROM [monit].[PROCEDIMENTOS] AS mp, [monit].[MALHAS] as mm"
			+ " WHERE (:nameSearch IS NULL OR mp.nome LIKE :nameSearch)" 
			+ " AND (:malhaSearch IS NULL OR mm.nome LIKE :malhaSearch)"
			+ " AND (:sistemaSearch IS NULL OR mp.sistema LIKE :sistemaSearch)"
			+ " AND (mp.malhaId = mm.malhaId)", nativeQuery = true)
	public List<ProcAvailable> findAllPageable(
			@Param("nameSearch") String nameSearch, 
			@Param("malhaSearch") String malhaSearch,
			@Param("sistemaSearch") String sistemaSearch);
	
	@Query(value = "SELECT DISTINCT mp.sistema FROM [monit].[PROCEDIMENTOS] AS mp", nativeQuery=true)
	public List<String> findSistemas();

}
