package com.procdashboard.Application.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.procdashboard.Application.Entity.ProcLogError;

public interface ProcLogErrorDao extends CrudRepository <ProcLogError, Integer> {
	
	@Query(value = "SELECT *" + 
			"  FROM [monit].[LOG_ERROR]" +
			"  WHERE logExecId = :logExecId ", nativeQuery = true)
	public List<ProcLogError> findByLogExecId(
			@Param("logExecId") Integer logExecId);
	
	public List<ProcLogError> findAll();
	
}
