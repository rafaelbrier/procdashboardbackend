package com.procdashboard.Application.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.procdashboard.Application.Entity.Malha;

public interface MalhaDao extends CrudRepository <Malha, Integer> {

	@Query(value = "SELECT DISTINCT m.nome FROM [monit].[MALHAS] AS m", nativeQuery=true)
	public List<String> findMalhasNome();
	
	@Query(value = "SELECT TOP 100 * FROM [monit].[MALHAS]", nativeQuery = true)
	public List<Malha> findMalhas();
}
