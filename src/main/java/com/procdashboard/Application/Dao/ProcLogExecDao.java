package com.procdashboard.Application.Dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.procdashboard.Application.Entity.ProcChartData;
import com.procdashboard.Application.Entity.ProcLogExec;

public interface ProcLogExecDao extends CrudRepository <ProcLogExec, Integer>  {
	
	@Query(value = "SELECT ple.logExecId As logExecId,"
			+ " ple.dataInicio AS dataInicio,"
			+ " ple.dataFim AS dataFim,"
			+ " ple.procId AS procId,"
			+ " ple.[status] AS status,"
			+ " ple.tempoExecucao AS tempoExecucao"
			+ " FROM [monit].[LOG_EXEC] AS ple"
			+ " LEFT JOIN [monit].[PROCEDIMENTOS] AS pa ON ple.procId = pa.procId"
			+ " LEFT JOIN [monit].[MALHAS] AS m ON pa.malhaId = m.malhaId"
			+ " WHERE (:nameSearch IS NULL OR pa.nome LIKE :nameSearch )" 
			+ " AND (:malhaSearch IS NULL OR m.nome LIKE :malhaSearch)"
			+ " AND (:jobSearch IS NULL OR pa.jobNome LIKE :jobSearch)"
			+ " AND (:sistemaSearch IS NULL OR pa.sistema LIKE :sistemaSearch)"
			+ " AND (:startDateSearch IS NULL OR CONVERT(VARCHAR(30), ple.dataInicio, 103) LIKE :startDateSearch )"
			+ " AND (:execStatusSearch IS NULL OR ple.[status] = :execStatusSearch )"
			+ " ORDER BY (CASE WHEN ple.[status]='R' THEN -1 END) DESC, ple.dataInicio DESC",
			countQuery = "SELECT COUNT(ple.logExecId) FROM [monit].[LOG_EXEC] AS ple",
			nativeQuery = true)
	public Page<ProcLogExec> findAll(
			@Param("nameSearch") String nameSearch, 
			@Param("startDateSearch") String startDateSearch, 
			@Param("malhaSearch") String malhaSearch,
			@Param("jobSearch") String jobSearch,
			@Param("sistemaSearch") String sistemaSearch,
			@Param("execStatusSearch") String execStatusSearch,
			Pageable pageable);
	
	
	@Query(value = "SELECT AVG(aple.tempoExecucao) AS tempoMedio FROM [monit].[LOG_EXEC] AS aple" + 
			" WHERE aple.procId = :procId AND (aple.dataInicio >= GETDATE()-90)", nativeQuery = true)
	public Integer findAverageTime(@Param("procId") Integer procId);
	
	
	
//	@Query(value = ";WITH cte AS (SELECT p.tempoExecucao, CAST(p.dataInicio AS DATE) AS dataInicio,"
//			+ " (SELECT COUNT(*) FROM dbo.ProcLogError AS e WHERE e.logExecId = p.logExecId) AS numeroErros"
//			+ "	 FROM dbo.ProcLogExec as p WHERE p.procId = :procId)"
//			+ " SELECT AVG(tempoExecucao) AS tempoExecucao, dataInicio, SUM(numeroErros) AS numeroErros"
//			+ "	FROM cte GROUP BY dataInicio", nativeQuery = true)
//	public List<ProcChartData> findChartData(@Param("procId") Integer procId);
	
	@Query(value = "SELECT AVG(tempoExecucao) AS tempoExecucao,"
			+ "	CAST(p.dataInicio AS DATE) as dataInicio,"
			+ " SUM(CASE WHEN p.[status] = 'E' THEN 1 ELSE 0 END) AS numeroErros,"
			+ " COUNT(p.logExecId) AS numeroExecucao"
			+ " FROM [monit].[LOG_EXEC] p WHERE p.procId = :procId"
			+ " GROUP BY CAST(p.dataInicio AS DATE)", nativeQuery = true)
	public List<ProcChartData> findChartData(@Param("procId") Integer procId);

		
}
