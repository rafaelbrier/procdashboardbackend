package com.procdashboard.Authentication.Controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.procdashboard.Authentication.Security.AuthenticationDTO;

@RestController
public class LoginController {


	@PostMapping("/login")
	public String login(@RequestBody AuthenticationDTO authentication) {
		// TODO Método criado apenas para aparecer no Swagger. Ver outra forma para exibir.
		return "redirect:/login";
	}
}
