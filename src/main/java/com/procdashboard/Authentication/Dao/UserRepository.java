package com.procdashboard.Authentication.Dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.procdashboard.Authentication.Entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer>{
	
	@Query("select u from User u left join fetch u.authorities where u.username = :username")
	User findByUsername(@Param("username") String username);
	
	@Transactional
	@Modifying
	@Query(value= 
		    "insert into user(username, password) values ('ZurichSVM', '$2a$10$q4j4J70b/xR0SJNPDGi9ueaOhAT1etb5/Xpt6yz4ODqCInSca/cFC');" + 
			"insert into authority(name, description) values ('ADMIN', 'Role admin');" + 
			"insert into user_authority(id_user, id_authority) values (1,1);", nativeQuery = true)
	void defaultUserInit();
}
