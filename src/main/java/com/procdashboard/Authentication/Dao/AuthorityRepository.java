package com.procdashboard.Authentication.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.procdashboard.Authentication.Entity.Authority;

@Repository
public interface AuthorityRepository extends CrudRepository<Authority, Integer> {

}
