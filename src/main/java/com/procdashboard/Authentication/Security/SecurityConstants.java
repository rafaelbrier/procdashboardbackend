package com.procdashboard.Authentication.Security;

public final class SecurityConstants {
	
	private SecurityConstants() {}
	
	// EXPIRATION_TIME = 10hr
	public static final long EXPIRATION_TIME = 30_600_000;
	public static final String SECRET = "StefaniniZurichMonitoring";
	public static final String AUTHORITIES_KEY = "authorities";
	public static final String TOKEN_PREFIX = "Bearer";
	public static final String AUTHORIZATION_HEADER_KEY = "Authorization";

}
