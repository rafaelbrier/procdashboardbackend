package com.procdashboard.Authentication.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import com.procdashboard.Authentication.Dao.UserRepository;
import com.procdashboard.Authentication.Entity.User;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public User save(User user) {
		return userRepository.save(user);
	}

	public User update(User user) {
		return this.save(user);
	}

	public Optional<User> findById(Integer id) {
		return userRepository.findById(id);
	}

	@Secured("ADMIN")
	public Iterable<User> findAll() {
		return userRepository.findAll();
	}

	public void delete(Integer id) {
		userRepository.deleteById(id);
	}

}
