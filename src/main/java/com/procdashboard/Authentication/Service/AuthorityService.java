package com.procdashboard.Authentication.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.procdashboard.Authentication.Dao.AuthorityRepository;
import com.procdashboard.Authentication.Entity.Authority;

@Service
public class AuthorityService {

	@Autowired
	private AuthorityRepository authorityRepositry;
	
	public Iterable<Authority> findAll() {
		return authorityRepositry.findAll();
	}
}
