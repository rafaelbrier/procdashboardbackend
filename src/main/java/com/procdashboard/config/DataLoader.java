package com.procdashboard.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.procdashboard.Authentication.Dao.UserRepository;

@Component
public class DataLoader implements ApplicationRunner {

	private UserRepository userRepository;

	@Autowired
	public DataLoader(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public void run(ApplicationArguments args) {
			this.userRepository.defaultUserInit();
//			BCryptPasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();
//			System.out.println(SecurityConfig.PASSWORD_ENCODER.encode("passwordToEncrypt"));

	}
}
