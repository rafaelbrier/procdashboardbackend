package com.procdashboard.config;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EntityScan("com.procdashboard.Authentication")
@EnableJpaRepositories(entityManagerFactoryRef = "authEntityManagerFactory",
    transactionManagerRef = "authTransactionManager", basePackages = {"com.procdashboard.Authentication.Dao"})
public class AuthDBConfig {

  @Bean(name = "authDataSource")
  @ConfigurationProperties(prefix = "auth.datasource")
  public DataSource dataSource() {
    return DataSourceBuilder.create().build();
  }

  @Bean(name = "authEntityManagerFactory")
  public LocalContainerEntityManagerFactoryBean authEntityManagerFactory(
      EntityManagerFactoryBuilder builder, @Qualifier("authDataSource") DataSource dataSource) {
	  	Map<String, Object> properties = new HashMap<String, Object>();
	    properties.put("hibernate.hbm2ddl.auto", "update");
    return builder.dataSource(dataSource)
    		.packages("com.procdashboard.Authentication.Entity")
    		.persistenceUnit("Authentication")
    		.properties(properties)
    		.build();
  }

  @Bean(name = "authTransactionManager")
  public PlatformTransactionManager authTransactionManager(
      @Qualifier("authEntityManagerFactory") EntityManagerFactory authEntityManagerFactory) {
    return new JpaTransactionManager(authEntityManagerFactory);
  }

}