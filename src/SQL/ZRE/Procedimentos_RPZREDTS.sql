USE [Resseguro]
GO

/*
Malha : 2 - RPZREDTS
Ids : 26 at� 50
*/

/*
ID: 29
Procedure: SP_Atualizar_Descricao_Formal_Filial
JOB: 03RPZREDT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   29, 'SP_Atualizar_Descricao_Formal_Filial',
	   2, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '06:10', '21:10',
	  '1', '30',
	  '1H',
	  '03RPZREDT#',
	  'Atualiza descri��o formal de Filial no BIT a partir de UPDATES com queries fixas contendo o c�digo da filial e nome dentro da procedure.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	  GETDATE()
	 );

/*
ID: 30
Procedure: SP_Carrega_Dados_Sise_Resseguro
JOB: 04RPZREDT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   30, 'SP_Carrega_Dados_Sise_Resseguro',
	   2, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '06:10', '21:10',
	  '1', '30',
	  '1H',
	  '04RPZREDT#',
	  'Atualiza tabelas quentes, a partir das frias, no BIT, de Sucursal, Filial, Ramo, Produto, Subproduto, Cobertura, Ap�lice, Ap�lice Item, Ap�lice Item Cobertura, Comiss�o, Corretor, Cong�nere, Parcela, s� para a Empresa 01 � SISE.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	  GETDATE()
	 );
	 
/*
ID: 33
Procedure: PR_Carrega_Dados_Premio_Para_Bit
JOB: 05RPZREDT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   33, 'PR_Carrega_Dados_Premio_Para_Bit',
	   2, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '06:10', '21:10',
	   '1', '30',
	  '1H',
	  '05RPZREDT#',
	  NULL,
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	  GETDATE()
	 );
	 
/*
ID: 34
Procedure: SP_Importa_Dados_Para_Resseguro
JOB: 06RPZREDT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   34, 'SP_Importa_Dados_Para_Resseguro',
	   2, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '06:10', '21:10',
	   '1', '30',
	  '1H',
	  '06RPZREDT#',
	  'Importar dados de ap�lices / endossos para o ZRE. Insere dados das tabelas quentes do BIT para Resseguro.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	  GETDATE()
	 );
	 
/*
ID: 35
Procedure: PR_Resseguro_Carrega_TCURR_01
JOB: 07RPZREDT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   35, 'PR_Resseguro_Carrega_TCURR_01',
	   2, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '06:10', '21:10',
	  '1', '30',
	  '1H',
	  '07RPZREDT#',
	  'Carregar informa��es de c�mbio.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	  GETDATE()
	 );
	 
/*
ID: 36
Procedure: PR_Resseguro_Carrega_Cormis_SP_01
JOB: 08RPZREDT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   36, 'PR_Resseguro_Carrega_Cormis_SP_01',
	   2, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '06:10', '21:10',
	  '1', '30',
	  '1H',
	  '08RPZREDT#',
	  'Carregar dados cormis das emiss�es de SP. Carrega dados de comiss�o das emiss�es de SP, Cormis_Apolice, a partir do banco BIG.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	  GETDATE()
	 );
	 
/*
ID: 37
Procedure: PR_Atualiza_Tabelas_Retrocessao
JOB: 09RPZREDT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   37, 'PR_Atualiza_Tabelas_Retrocessao',
	   2, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '06:10', '21:10',
	   '1', '30',
	  '1H',
	  '09RPZREDT#',
	  'Atualiza tabelas para empresa 999 (Filial, Ramo, Produto).',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	  GETDATE()
	 );

/*
ID: 38
Procedure: AtualizaGrupoRamo
JOB: 10RPZREDT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   38, 'AtualizaGrupoRamo',
	   2, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '06:10', '21:10',
	   '1', '30',
	  '1H',
	  '10RPZREDT#',
	  'Atualiza Grupo Ramo a partir do BIG.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	  GETDATE()
	 );
	 
/*
ID: 39
Procedure: SP_Atualiza_Accouting_Group
JOB: 11RPZREDT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   39, 'SP_Atualiza_Accouting_Group',
	   2, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '06:10', '21:10',
	   '1', '30',
	  '1H',
	  '11RPZREDT#',
	  ' Atualiza tabela Cob_Account_Group.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	  GETDATE()
	 );
