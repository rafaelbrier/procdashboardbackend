USE [Resseguro]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [monit].[LOG_ERROR](
	[logErrorId] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [varchar](40) NULL,
	[criadoEm] [datetime2](0) NULL,
	[linha] [varchar](15) NULL,
	[critico] [varchar](15) NULL,
	[logExecId] [int] NULL,
	[mensagem] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[logErrorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [monit].[LOG_ERROR]  WITH CHECK ADD  CONSTRAINT [FK_LogExec_LogError] FOREIGN KEY([logExecId])
REFERENCES [monit].[LOG_EXEC] ([logExecId])
GO

ALTER TABLE [monit].[LOG_ERROR] CHECK CONSTRAINT [FK_LogExec_LogError]
GO
