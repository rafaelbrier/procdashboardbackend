USE [Resseguro]
GO

--PROCEDIMENTOS
CREATE OR ALTER PROCEDURE dbo.PR_Carrega_Sinistros_SISE AS BEGIN
	SET NOCOUNT ON

 -- Buscar Id Do Procedimento
SELECT * -- procId as ID, nome as Nome
FROM [monit].PROCEDIMENTOS
WHERE nome LIKE 'PR_Resseguro_Processa_Lotes_(528)'

SELECT procId as ID, nome as Nome
FROM [monit].PROCEDIMENTOS
ORDER BY nome asc
 ----------------------------- 
/************************************************
Data: 03/04/2019
Autor: Rafael Marcio
Altera��o: Adicionado c�digo para monitoramento
************************************************/
/* MONITORAMENTO -> C�digo de In�cio */
DECLARE @logExecStartPointId INT;
EXECUTE [monit].[SP_START] 1, @logExecStartPointId OUTPUT; -- Input: ID do procedimento, Sa�da: ID da execu��o
/*-----------------*/
	
	BEGIN TRY
		DECLARE @i INT = 1;
		WHILE (@i <= 2)	 BEGIN  WAITFOR DELAY '00:00:01';  SET  @i = @i + 1; END 
		--RAISERROR('ERROR CR�TICO',16,1)
	END TRY
	/* MONITORAMENTO -> Loga Erro Cr�tico */
	BEGIN CATCH
		EXECUTE [monit].[SP_LOG_ERROR] @logExecStartPointId, 1, ''; 
	END CATCH;

	BEGIN TRY
		DECLARE @i2 INT = 1;
		WHILE (@i2 <= 3)	 BEGIN  WAITFOR DELAY '00:00:01';  SET  @i2 = @i2 + 1; END
		RAISERROR('ERROR CR�TICO',16,1)
	END TRY
	/* MONITORAMENTO -> Loga Erro N�o Cr�tico */
	BEGIN CATCH	EXECUTE [monit].[SP_LOG_ERROR] @logExecStartPointId, 0; END CATCH
	 
	/* MONITORAMENTO -> Finaliza��o */	
	EXECUTE [monit].[SP_END]  @logExecStartPointId;
END

GO

EXECUTE dbo.PR_Carrega_Sinistros_SISE

  
END TRY
/* MONITORAMENTO -> Loga Erro N�o Cr�tico */
BEGIN CATCH	EXECUTE [monit].[SP_LOG_ERROR] @logExecStartPointId, 0; END CATCH

END TRY
/* MONITORAMENTO -> Loga Erro N�o Cr�tico */
BEGIN CATCH
	EXECUTE [monit].[SP_LOG_ERROR] @logExecStartPointId, 0, 'Erro na execu��o da procedure chamada: Calcula_Retrocessao_Resseguro_Plano_Desconto.';
END CATCH 

END TRY
/* MONITORAMENTO -> Loga Erro Cr�tico */
BEGIN CATCH
	SET @MsgErro = 'Erro No Select Apolice no Enquadramento Resseguro !';
	EXECUTE [monit].[SP_LOG_ERROR] @logExecStartPointId, 1, @MsgErro; 
	GOTO TrataErro;
END CATCH;  

/* MONITORAMENTO -> Loga Erro Cr�tico */
EXECUTE [monit].[SP_LOG_ERROR] @logExecStartPointId, 1, @MsgErro, @Erro, 'Script -- 186';

/* MONITORAMENTO -> Loga Erro N�o Cr�tico */
EXECUTE [monit].[SP_LOG_ERROR] @logExecStartPointId, 0, @MsgErro, @Erro, 'Script -- 186';  



