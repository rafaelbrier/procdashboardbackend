USE [Resseguro]
GO

/*
Malha : 1 - RPZRESID
Ids : 1 at� 25
*/

/*
ID: 1
Procedure: PR_Carrega_Sinistros_SISE
JOB: 01RPZRESI#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   1, 'PR_Carrega_Sinistros_SISE',
	   1, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '08:30', '23:30',
	  '1', '30',
	  '1H',
	  '01RPZRESI#',
	  'Carrega sinistro para tabelas oficiais na base BIT.',
	  '00:01:00', NULL,
	  'Do dia 26 at� o final do m�s o intervalo de execu��o � reduzido para 15 minutos.',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );

/*
ID: 2
Procedure: PR_Resseguro_Processa_Lotes
JOB: 02RPZRESI#, 05RPZREDT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   2, 'PR_Resseguro_Processa_Lotes',
	   1, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', 'Variado', 'Variado',
	  '1', '30',
	  '1H',
	  '02RPZRESI#, 05RPZREDT#',
	  'Validar e processar lote recebido.',
	  '00:01:00', NULL,
	   'Para o Job 02RPZRESI#: Executa do dia 26 at� o final do m�s com intervalo de execu��o reduzido para 15 minutos. Hor�rio de Execu��o 08:30 �s 23:30.
	    Para o Job 05RPZREDT#: Hor�rio de Execu��o 06:10 �s 21:10.',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
		GETDATE()
	 );
	 
/*
ID: 3
Procedure: PR_Resseguro_Valida_Lote_Enviado
JOB: 02RPZRESI#, 05RPZREDT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   3, 'PR_Resseguro_Valida_Lote_Enviado',
	   1, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', 'Variado', 'Variado',
	  '1', '30',
	  '1H',
	  '02RPZRESI#',
	  NULL,
	  '00:01:00', NULL,
	   'Para o Job 02RPZRESI#: Executa do dia 26 at� o final do m�s com intervalo de execu��o reduzido para 15 minutos. Hor�rio de Execu��o 08:30 �s 23:30.
	    Para o Job 05RPZREDT#: Hor�rio de Execu��o 06:10 �s 21:10.',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
		GETDATE()
	 );
	 
/*
ID: 4
Procedure: PR_Importa_Sinistros_Para_Bit
JOB: 02RPZRESI#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   4, 'PR_Importa_Sinistros_Para_Bit',
	   1, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '08:30', '23:30',
	  '1', '30',
	  '1H',
	  '02RPZRESI#',
	  NULL,
	  '00:01:00', NULL,
	  'Do dia 26 at� o final do m�s o intervalo de execu��o � reduzido para 15 minutos.',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
		GETDATE()
	 );

/*
ID: 5
Procedure: PR_Importa_Sinistros_Bit_Para_Resseguro
JOB: 03RPZRESI#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   5, 'PR_Importa_Sinistros_Bit_Para_Resseguro',
	   1, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '08:30', '23:30',
	  '1', '30',
	  '1H',
	  '03RPZRESI#',
	  'Retorno c�lculo Resseguro para SISE Pr�mio.',
	  '00:01:00', NULL,
	  'Do dia 26 at� o final do m�s o intervalo de execu��o � reduzido para 15 minutos.',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
		GETDATE()
	 );

/*
ID: 6
Procedure: PR_Resseguro_Carrega_Estimativas_Incremental
JOB: 04RPZRESI#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   6, 'PR_Resseguro_Carrega_Estimativas_Incremental',
	   1, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '08:30', '23:30',
	  '1', '30',
	  '1H',
	  '04RPZRESI#',
	  'Carrega as estimativas recebidas calculando a �ltima posi��o. Retorno estimativa.',
	  '00:01:00', NULL,
	  'Do dia 26 at� o final do m�s o intervalo de execu��o � reduzido para 15 minutos.',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
		GETDATE()
	 );
	
/*
ID: 7
Procedure: PR_Resseguro_Processa_Composicao_Diaria
JOB: 05RPZRESI#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   7, 'PR_Resseguro_Processa_Composicao_Diaria',
	   1, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '08:30', '23:30',
	  '1', '30',
	  '1H',
	  '05RPZRESI#',
	  'Processar a composi��o de forma di�ria. Retorno estimativa Sise.',
	  '00:01:00', NULL,
	  'Do dia 26 at� o final do m�s o intervalo de execu��o � reduzido para 15 minutos.',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
		GETDATE()
	 );
	
/*
ID: 8
Procedure: PR_Resseguro_Aloca_Calculo_Estimativa_Retorno
JOB: 06RPZRESI#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   8, 'PR_Resseguro_Aloca_Calculo_Estimativa_Retorno',
	   1, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '08:30', '23:30',
	  '1', '30',
	  '1H',
	  '06RPZRESI#',
	  ' Recuperar o c�lculo da ultima estimativa para a estimativa de resseguro recebida do SISE. Grava atualiza��o SISE.',
	  '00:01:00', NULL,
	  'Do dia 26 at� o final do m�s o intervalo de execu��o � reduzido para 15 minutos.',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
		GETDATE()
	 );
	 
	 
	 