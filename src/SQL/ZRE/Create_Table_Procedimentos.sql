USE [Resseguro]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [monit].[PROCEDIMENTOS](
	[procId] [int] UNIQUE NOT NULL,
	[banco] [varchar](100) NULL,
	[criadoEm] [datetime2](0) NULL,
	[criticidade] [varchar](100) NULL,
	[descricao] [text] NULL,
	[diaFim] [varchar](100) NULL,
	[diaInicio] [varchar](100) NULL,
	[fechamento] [varchar](100) NULL,
	[horaFim] [varchar](100) NULL,
	[horaInicio] [varchar](100) NULL,
	[intervaloExec] [varchar](100) NULL,
	[jobNome] [varchar](255) NULL,
	[nome] [varchar](255) NULL,
	[observacao] [text] NULL,
	[erroObservacao] [text] NULL,
	[periodicidade] [varchar](100) NULL,
	[sistema] [varchar](100) NULL,
	[tempoEstimado] [datetime2](0) NULL,
	[ultimaExecucao] [datetime2](0) NULL,
	[malhaId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[procId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [monit].[PROCEDIMENTOS]  WITH CHECK ADD  CONSTRAINT [FK_Malha_Procedimentos] FOREIGN KEY([malhaId])
REFERENCES [monit].[MALHAS] ([malhaId])
GO

ALTER TABLE [monit].[PROCEDIMENTOS] CHECK CONSTRAINT [FK_Malha_Procedimentos]
GO
