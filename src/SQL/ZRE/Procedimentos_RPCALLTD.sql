USE [Resseguro]
GO

/*
Malha : 3 - RPCALLTD
Ids : 51 at� 100
*/

/*
ID: 51
Procedure: PR_Calcula_Resseguro_Retrocessao
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   51, 'PR_Calcula_Resseguro_Retrocessao',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'C�lculo pr�mio, (Chama_Calculo_Resseguro).',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );

/*
ID: 52
Procedure: Enquadra_Calcula_Resseguro01
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   52, 'Enquadra_Calcula_Resseguro01',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Bloqueio de calculo para resseguros migrados.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
/*
ID: 53
Procedure: PR_Calcula_Retrocessao
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   53, 'PR_Calcula_Retrocessao',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Calcula retrocess�o.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
/*
ID: 54
Procedure: SP_Deleta_Retrocessao
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   54, 'SP_Deleta_Retrocessao',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Deleta retrocess�o.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
	 
/*
ID: 55
Procedure: PR_Resseguro_Verifica_Calculo_Endosso
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   55, 'PR_Resseguro_Verifica_Calculo_Endosso',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Efetuar a composi��o de coberturas de um documento (Endosso).',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
	 
/*
ID: 56
Procedure: PR_Resseguro_Compor_Itens
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   56, 'PR_Resseguro_Compor_Itens',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Gerar a composi��o do LMI e pr�mio tarifa dos itens, prepara o documento para enquadramento e c�lculo.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
	 
/*
ID: 57
Procedure: Enquadra_Resseguro02
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   57, 'Enquadra_Resseguro02',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Localizar o contrato que ser� utilizado para calcular o resseguro.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
/*
ID: 58
Procedure: Calcula_Resseguro
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   58, 'Calcula_Resseguro',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Efetuar o c�lculo do pr�mio de resseguro de um documento. Conforme as regras WAQS.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
/*
ID: 59
Procedure: SP_Resseg_Coberturas_Retrocessao
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   59, 'SP_Resseg_Coberturas_Retrocessao',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Bloqueio de c�lculo para resseguros migrados.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
/*
ID: 60
Procedure: SP_Busca_Parametro_Empresa_Circular_395
JOB: 01RPBKPMD#, 01RPRESLT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   60, 'SP_Busca_Parametro_Empresa_Circular_395',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', NULL, NULL,
	  '1', '30',
	  NULL,
	  '01RPBKPMD#, 01RPRESLT#',
	  'Buscar data de corte para contabiliza��o WAQS (Circular 395).',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
/*
ID: 61
Procedure: Gera_Cancelamento_Reativacao01
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   61, 'Gera_Cancelamento_Reativacao01',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Processar cancelamento ou reativa��o do ap�lice/endosso.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
/*
ID: 62
Procedure: SP_Resseg_Compor_Item_Retrocessao
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   62, 'SP_Resseg_Compor_Item_Retrocessao',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Compor item retrocess�o.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
/*
ID: 63
Procedure: SP_Inclui_Reversao_Premio_Parcelas
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   63, 'SP_Inclui_Reversao_Premio_Parcelas',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Bloqueio de c�lculo para resseguros migrados.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
/*
ID: 64
Procedure: Calcula_Resseguro_Plano_Desconto
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   64, 'Calcula_Resseguro_Plano_Desconto',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Efetuar o c�lculo do pr�mio a descontar.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
/*
ID: 65
Procedure: Calcula_Retrocessao_Resseguro_Plano_Desconto
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   65, 'Calcula_Retrocessao_Resseguro_Plano_Desconto',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Efetuar o c�lculo do pr�mio a descontar  no c�lculo de retrocess�o.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
/*
ID: 66
Procedure: Inclui_Resseguro_Parcelas
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   66, 'Inclui_Resseguro_Parcelas',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Inclui Resseguro Parcelas.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );

/*
ID: 67
Procedure: SP_Busca_Parametro_Empresa_Contabiliza_ZRe
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   67, 'SP_Busca_Parametro_Empresa_Contabiliza_ZRE',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Buscar data de corte para contabiliza��o WAQS (Circular 395).',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
/*
ID: 68
Procedure: SP_Atualiza_Rateio_Coberturas
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   68, 'SP_Atualiza_Rateio_Coberturas',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Atualiza Rateio Coberturas.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
	 
/*
ID: 69
Procedure: SP_Inclui_Reversao_Premio_Parcelas_Cancelamento
JOB: 01RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   69, 'SP_Inclui_Reversao_Premio_Parcelas_Cancelamento',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '01RPBKPMD#',
	  'Bloqueio de calculo para resseguro migrados.',
	  '00:01:00', NULL,
	  NULL,
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
/*
ID: 71
Procedure: PR_Sin_Calculo_Recuperacao_Resseguro01
JOB: 02RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   71, 'PR_Sin_Calculo_Recuperacao_Resseguro01',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '02RPBKPMD#',
	  'Executar o c�lculo de resseguro em lote de pagamentos de sinistros. � utilizada na p�gina (CALCULO ESTIMATIVA RECUPERA��O ORDEM DE PAGAMENTO EM LOTE) e na procedure (Chama_Calculo_Resseguro_Recuperacoes_Sinistro) que
	  faz parte do job (Resseguro - Calculo em Lote Sinistros).',
	  '00:01:00', NULL,
	  'Di�rio, com execu��o �nica e depende do predecessor que roda �s 12:15, (01RPBKPMD#).',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
	 
/*
ID: 72
Procedure: Chama_Calculo_Resseguro_Recuperacoes_Sinistro
JOB: 02RPBKPMD#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   72, 'Chama_Calculo_Resseguro_Recuperacoes_Sinistro',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '12:15', NULL,
	  '1', '30',
	  '�nica Execu��o',
	  '02RPBKPMD#',
	  'Executar o c�lculo de resseguro em lote de pagamentos de sinistros. � utilizada na p�gina (CALCULO ESTIMATIVA RECUPERA��O ORDEM DE PAGAMENTO EM LOTE) e na procedure (Chama_Calculo_Resseguro_Recuperacoes_Sinistro) que
	  faz parte do job (Resseguro - Calculo em Lote Sinistros).',
	  '00:01:00', NULL,
	  'Di�rio, com execu��o �nica e depende do predecessor que roda �s 12:15, (01RPBKPMD#).',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
	 
/*
ID: 73
Procedure: PR_ErrorMessage
JOB: 01RPRESLT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   73, 'PR_ErrorMessage',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '08:35', '20:35',
	  '1', '30',
	  '1H',
	  '01RPRESLT#',
	  NULL,
	  '00:01:00', NULL,
	  'Di�rio, com execu��o �nica e depende do predecessor que roda �s 12:15, (01RPBKPMD#).',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
/*
ID: 74
Procedure: Deleta_Estimativa_Retrocessao
JOB: 01RPRESLT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   74, 'Deleta_Estimativa_Retrocessao',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '08:35', '20:35',
	  '1', '30',
	  '1H',
	  '01RPRESLT#',
	  'Deleta Estimativa Retrocess�o.',
	  '00:01:00', NULL,
	  'Di�rio, com execu��o �nica e depende do predecessor que roda �s 12:15, (01RPBKPMD#).',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
	 
/*
ID: 75
Procedure: PR_Sin_Calculo_Estimativa_Recuperacao_Resseguro_PlanoER_01
JOB: 01RPRESLT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   75, 'PR_Sin_Calculo_Estimativa_Recuperacao_Resseguro_PlanoER_01',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '08:35', '20:35',
	  '1', '30',
	  '1H',
	  '01RPRESLT#',
	  'C�lculo Estimativa Recupera��o Resseguro PlanoER 01.',
	  '00:01:00', NULL,
	  'Di�rio, com execu��o �nica e depende do predecessor que roda �s 12:15, (01RPBKPMD#).',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
	 
/*
ID: 76
Procedure: PR_Sin_Calculo_Estimativa_Recuperacao_Resseguro_PlanoQT_01
JOB: 01RPRESLT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   76, 'PR_Sin_Calculo_Estimativa_Recuperacao_Resseguro_PlanoQT_01',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '08:35', '20:35',
	  '1', '30',
	  '1H',
	  '01RPRESLT#',
	  'C�lculo Estimativa Recupera��o Resseguro PlanoQT 01.',
	  '00:01:00', NULL,
	  'Di�rio, com execu��o �nica e depende do predecessor que roda �s 12:15, (01RPBKPMD#).',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
	 
/*
ID: 77
Procedure: PR_Sin_Calculo_Estimativa_Recuperacao_Resseguro_PlanoED_01
JOB: 01RPRESLT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   77, 'PR_Sin_Calculo_Estimativa_Recuperacao_Resseguro_PlanoED_01',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '08:35', '20:35',
	  '1', '30',
	  '1H',
	  '01RPRESLT#',
	  'C�lculo Estimativa Recupera��o Resseguro PlanoED 01.',
	  '00:01:00', NULL,
	  'Di�rio, com execu��o �nica e depende do predecessor que roda �s 12:15, (01RPBKPMD#).',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
/*
ID: 78
Procedure: PR_Sin_Calculo_Estimativa_Recuperacao_Resseguro_Grava
JOB: 01RPRESLT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   78, 'PR_Sin_Calculo_Estimativa_Recuperacao_Resseguro_Grava',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '08:35', '20:35',
	  '1', '30',
	  '1H',
	  '01RPRESLT#',
	  'C�lculo Estimativa Recupera��o Resseguro Grava.',
	  '00:01:00', NULL,
	  'Di�rio, com execu��o �nica e depende do predecessor que roda �s 12:15, (01RPBKPMD#).',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );

/*
ID: 79
Procedure: PR_Sin_Calculo_Estimativa_Recuperacao_Resseguro
JOB: 01RPRESLT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   79, 'PR_Sin_Calculo_Estimativa_Recuperacao_Resseguro',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '08:35', '20:35',
	  '1', '30',
	  '1H',
	  '01RPRESLT#',
	  'C�lculo Estimativa Recupera��o Resseguro.',
	  '00:01:00', NULL,
	  'Di�rio, com execu��o �nica e depende do predecessor que roda �s 12:15, (01RPBKPMD#).',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
	 
/*
ID: 80
Procedure: Chama_Calculo_Resseguro_Estimativas_Sinistro
JOB: 01RPRESLT#
Alterada para monitoramento: N�O
*/
INSERT INTO [monit].[PROCEDIMENTOS] (
		procId, nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, horaFim,
		diaInicio, diaFim,
		intervaloExec,
		jobNome,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		erroObservacao,
		criadoEm
	)
	VALUES(
	   80, 'Chama_Calculo_Resseguro_Estimativas_Sinistro',
	   3, 'ZRE', 'Resseguro',
	  'M', 'S',
	  'D', '08:35', '20:35',
	  '1', '30',
	  '1H',
	  '01RPRESLT#',
	  'Executar o calculo em lote de estimativas do resseguro de sinistros.',
	  '00:01:00', NULL,
	  'Di�rio, com execu��o �nica e depende do predecessor que roda �s 12:15, (01RPBKPMD#).',
	  'Em caso de erro ou cancelamento, enviar email para o analista respons�vel.',
	   GETDATE()
	 );
