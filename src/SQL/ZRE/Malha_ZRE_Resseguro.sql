USE [Resseguro]
GO

INSERT INTO [monit].[MALHAS] (malhaId, nome, descricao)
VALUES (
1,
'RPZRESID',
'Malha respons�vel pela integra��o de Sinistros SISE/TIA para o BIT/ZRE. Esta malha � composta de (8) Jobs, possui uma periodicidade di�ria, sendo que entre os dias 01 e 25 os intervalos de execu��o s�o de 1 em 1 hora, e do dia 26 at� o final do m�s esse intervalo � reduzido para 15 minutos para n�o afetar o fechamento. A janela de opera��o da malha � de 08h30h. �s 23:30h.'
);
INSERT INTO [monit].[MALHAS] (malhaId, nome, descricao)
VALUES (
2,
'RPZREDTS',
'Malha respons�vel pela integra��o de Pr�mios SISE para o BIT/ZRE. Esta malha � composta de 11 Jobs, possui uma periodicidade di�ria, com intervalos de execu��o de 1 em 1 hora. A janela de opera��o da malha � de 06h10h. �s 21:10h.'
);
INSERT INTO [monit].[MALHAS] (malhaId, nome, descricao)
VALUES (
3,
'RPCALLTD',
NULL
);