USE [Resseguro]
GO

/****** Object:  Table [dbo].[ProcLogExec]    Script Date: 01/04/2019 08:34:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [monit].[LOG_EXEC](
	[logExecId] [int] IDENTITY(1,1) NOT NULL,
	[dataFim] [datetime2](0) NULL,
	[dataInicio] [datetime2](0) NULL,
	[procId] [int] NULL,
	[status] [varchar](100) NULL,
	[tempoExecucao] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[logExecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [monit].[LOG_EXEC]  WITH CHECK ADD  CONSTRAINT [FK_Procedimentos_LogExec] FOREIGN KEY([procId])
REFERENCES [monit].[PROCEDIMENTOS] ([procId])
GO

ALTER TABLE [monit].[LOG_EXEC] CHECK CONSTRAINT [FK_Procedimentos_LogExec]
GO
