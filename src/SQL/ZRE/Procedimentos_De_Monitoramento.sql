USE [Resseguro];
GO

CREATE PROCEDURE [monit].[SP_START] @procId              INT, 
                                           @logExecStartPointId INT OUTPUT
AS
    BEGIN TRY
        DECLARE @startPointLogExecId INT= NULL; --Identificador do Log de Execu��o do Procedimento

        /* Loga o in�cio do procedimento */

        DECLARE @IdentityOutput TABLE(ID INT);
        INSERT [monit].[LOG_EXEC]
        (procId, 
         dataInicio, 
         dataFim, 
         [status], 
         tempoExecucao
        )
        OUTPUT INSERTED.logExecId
               INTO @IdentityOutput
        VALUES
        (@procID, 
         GETDATE(), 
         NULL, 
         'R', 
         NULL
        );
        SET @logExecStartPointId =
        (
            SELECT ID
            FROM @IdentityOutput
        );
    END TRY
    BEGIN CATCH
    END CATCH;
GO

CREATE PROCEDURE [monit].[SP_END] @logExecStartPointId INT
AS
    BEGIN TRY
        DECLARE @execStatus CHAR(1)= 'S';
        DECLARE @elapsedTime INT= NULL;
        DECLARE @procId INT= NULL;
		DECLARE @hasError INT = NULL;
        
        SET @procId =
        (
            SELECT TOP 1 procId
            FROM [monit].[LOG_EXEC]
            WHERE logExecId = @logExecStartPointId
        );

        /* Verifica se Tem Erro para Mudar o Status */
		SET @hasError = 
		(
			SELECT COUNT(logErrorId) 
			FROM [monit].[LOG_ERROR]
			WHERE logExecId = @logExecStartPointId
		)
		IF (@hasError <> 0) BEGIN
		  SET @execStatus = 'E';
		END

		/* Finaliza o Procedimento */
       UPDATE [monit].[LOG_EXEC]
          SET 
              dataFim = GETDATE(), 
              [status] = @execStatus, 
              tempoExecucao = ABS(DATEDIFF(millisecond, i.dataInicio, GETDATE()))
        FROM
        (
            SELECT TOP 1 dataInicio                          
            FROM [monit].[LOG_EXEC]
            WHERE logExecId = @logExecStartPointId
        ) AS i
        WHERE [monit].[LOG_EXEC].logExecId = @logExecStartPointId;
        IF(@procId IS NOT NULL)
            BEGIN
                UPDATE [monit].[PROCEDIMENTOS]
                  SET 
                      ultimaExecucao = GETDATE()
                WHERE procId = @procId;
        END;
    END TRY
    BEGIN CATCH
    END CATCH;
GO

CREATE PROCEDURE [monit].[SP_LOG_ERROR] @logExecStartPointId INT, 
											   @endExecution INT = 0,
                                               @errMessage          VARCHAR(4000) = NULL,
                                               @errCodigo           VARCHAR(40)  = NULL, 
                                               @errLine             VARCHAR(15)   = NULL 
AS
    BEGIN TRY
		DECLARE @critico CHAR(1) = 'N';
		
		IF (@endExecution <> 0) BEGIN
			SET @critico = 'S';
		END
		
		IF(@errCodigo IS NULL)  SET @errCodigo = CAST(ERROR_NUMBER() AS VARCHAR(40));
        IF(@errLine IS NULL)  SET @errLine = CAST(ERROR_LINE() AS VARCHAR(15));
        IF(@errMessage IS NULL)  SET @errMessage = ERROR_MESSAGE();
		IF(@errMessage IS NOT NULL AND ERROR_MESSAGE() IS NOT NULL)
			SET @errMessage = 'System_MSG: ' + ERROR_MESSAGE() + ' -- Author_MSG: ' + @errMessage;
						 
        INSERT INTO [monit].[LOG_ERROR]
        (logExecID, 
         codigo, 
		 critico,
         linha, 
         mensagem, 
         criadoEm
        )
        VALUES
        (@logExecStartPointId, 
         @errCodigo, 
		 @critico,
         @errLine, 
         @errMessage, 
         GETDATE()
        );

		IF (@endExecution <> 0) BEGIN
			EXECUTE [monit].[SP_END] @logExecStartPointId;
		END

    END TRY
    BEGIN CATCH
	END CATCH;