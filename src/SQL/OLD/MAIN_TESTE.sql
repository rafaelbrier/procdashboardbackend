USE [DB_Testes]
GO

--PROCEDIMENTOS
CREATE OR ALTER PROCEDURE dbo.sp_procedure_11 AS BEGIN
    /*C�digo de In�cio*/
	SET NOCOUNT ON
	DECLARE @procedureName NVARCHAR(100), @procedureDB NVARCHAR(20),
	 @procId INT, @logExecStartPointId INT, @notRegistered_FLAG BIT,	@tStart DATETIME;
	SET @procedureDB =  ORIGINAL_DB_NAME(); -- COLOCAR DENTRO DO START
	SET @tStart = GETDATE(); --sair
	SET @procedureName =  OBJECT_NAME(@@PROCID);
	EXECUTE mainprocedures.sp_START @procedureName, @procedureDB, @procId OUTPUT, @logExecStartPointId OUTPUT, @notRegistered_FLAG OUTPUT;
	/*-----------------*/
	
	BEGIN TRY
		/* O C�digo do procedimento vai aqui */
		DECLARE @i INT = 1;

			WHILE (@i <= 200)
			 BEGIN
			  WAITFOR DELAY '00:00:01';
	  
			 SET  @i = @i + 1;
			END 
		/*-----------------------------*/
		--RAISERROR	('ERRO DE TESTE', 16, 1)
		/* Finaliza��o Sem Erro*/
		EXECUTE mainprocedures.sp_END  @procId, @logExecStartPointId, @tStart, 0, @notRegistered_FLAG;
	END TRY
	
	
	BEGIN CATCH
	  /* Finaliza��o Com Erro */
	  EXECUTE mainprocedures.sp_END  @procId, @logExecStartPointId, @tStart, 1, @notRegistered_FLAG;
	  DECLARE @errNumber INT = ERROR_NUMBER(), @errSeverity INT = ERROR_SEVERITY(), @errState INT = ERROR_STATE(),
			  @errLine INT = ERROR_LINE(), @errMessage NVARCHAR(4000) = ERROR_MESSAGE();
	  EXECUTE mainprocedures.sp_LogError @procId, @logExecStartPointId, @errNumber, @errSeverity, @errState, @errLine, @errMessage;
	  THROW	  
	END CATCH
END

GO

EXECUTE dbo.sp_procedure_11