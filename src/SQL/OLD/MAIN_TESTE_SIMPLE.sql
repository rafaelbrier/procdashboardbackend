USE [Resseguro]
GO

--PROCEDIMENTOS
CREATE OR ALTER PROCEDURE dbo.PR_Carrega_Sinistros_SISE AS BEGIN
	SET NOCOUNT ON
/************************************************
Data: 03/04/2019
Autor: Rafael Marcio
Modificado: Adicionado c�digo para monitoramento
************************************************/
    /*C�digo de In�cio*/
	DECLARE @logExecStartPointId INT;
	EXECUTE [monit].sp_START 1, @logExecStartPointId OUTPUT; -- Input: ID do procedimento, Sa�da: ID da execu��o
	/*-----------------*/
	
	BEGIN TRY
		/* O C�digo do procedimento vai aqui */
		DECLARE @i INT = 1;

			WHILE (@i <= 1)
			 BEGIN
			  WAITFOR DELAY '00:00:01';
	  
			 SET  @i = @i + 1;
			END 
		/*-----------------------------*/
		RAISERROR	('ERRO DE TESTE', 16, 1)
		
		/* Finaliza��o Sem Erro*/
		EXECUTE [monit].sp_END  @logExecStartPointId;
	END TRY
	
	
	BEGIN CATCH
	  /* Finaliza��o Com Erro */
	  DECLARE @errCodigo INT = ERROR_NUMBER(), @errLine INT = ERROR_LINE(), @errMessage NVARCHAR(4000) = ERROR_MESSAGE();
	  EXECUTE [monit].sp_END  @logExecStartPointId, @errCodigo, @errLine, @errMessage;
	END CATCH

END

GO

EXECUTE dbo.PR_Carrega_Sinistros_SISE