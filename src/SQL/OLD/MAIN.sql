USE [DB_Testes]
GO

CREATE OR ALTER PROCEDURE mainprocedures.sp_START
	@procedureName NVARCHAR(100),
	@procedureDB NVARCHAR(20),
	@procId INT OUTPUT,
	@logExecStartPointId INT OUTPUT,
	@notRegistered_FLAG BIT OUTPUT
AS
BEGIN TRY
	DECLARE @__procedureId table ( ID int ); --Tabela Auxiliar
	DECLARE @procedureId NVARCHAR(100) = NULL; --Identificador do Procedimento
	DECLARE @__fechamento NVARCHAR(10) = NULL;
	DECLARE @startPointLogExecId INT =  NULL; --Identificador do Log de Execu��o do Procedimento
	DECLARE @procNotRegistered_FLAG BIT = 0;

	/* Busca no banco de dados o Id do procedimento */
	SET @procedureId = (SELECT TOP 1 procId FROM dbo.ProcAvailable AS cp WHERE cp.[nome] = @procedureName);
	SET @__fechamento = (SELECT TOP 1 fechamento FROM dbo.ProcAvailable AS cp WHERE cp.[nome] = @procedureName);

	/* Se o procedimento n�o estiver cadastrado na tabela
		O mesmo ser� cadastrado de forma b�sica e a flag de erro levantada para relatar 
		que o processo deve ser cadastro corretamente na tabela	
	 */ 
	IF (@procedureId IS NULL) BEGIN
		INSERT dbo.ProcAvailable (nome, banco)
			OUTPUT INSERTED.procId
					INTO @__procedureId
					VALUES (@procedureName, @procedureDB);
			
		SET @procedureId = (SELECT ID FROM @__procedureId);
		SET @procNotRegistered_FLAG = 1;
	END

	IF (@__fechamento IS NULL)
		SET @procNotRegistered_FLAG = 1;

	/* Loga o in�cio do procedimento */
	EXECUTE mainprocedures.sp_LogExec
			@procId = @procedureId,
			@startPointId_IN = NULL,
			@execStatus = 'R',
			@executionTime = NULL,
			@startPointId_OUT = @startPointLogExecId OUTPUT;
	

	/* Se o procedimento n�o estava registrado loga como erro avisando */
	IF (@procNotRegistered_FLAG = 1) BEGIN
		EXECUTE mainprocedures.sp_LogError
				@procID = @procedureId,
				@logExecID = @startPointLogExecId,
				@errNumber = 50000,
				@errSeverity = 0,
				@errState = 0,
				@errLine = 0,
				@errMessage = 'Procedimento requer cadastro.'
	END
			
	SET @procId = @procedureId;
	SET @logExecStartPointId = @startPointLogExecId;
	SET @notRegistered_FLAG = @procNotRegistered_FLAG;
END TRY
BEGIN CATCH
END CATCH
GO

CREATE OR ALTER PROCEDURE mainprocedures.sp_END
	@procId INT,
	@logExecStartPointId INT,
	@tStart DATETIME,
	@errFlag BIT,
	@notRegistered_FLAG BIT
AS
BEGIN TRY
	DECLARE @execStatus CHAR(1) = 'S';
	DECLARE @elapsedTime INT = NULL;
	DECLARE @avgTime INT;

	SET @elapsedTime = DATEDIFF(millisecond, @tStart, GETDATE());
	
	/* Loga o fim do Procedimento */
	IF @errFlag = 1
		SET @execStatus = 'E';
	ELSE IF @notRegistered_FLAG = 1
		SET @execStatus = 'W';
	
	EXECUTE mainprocedures.sp_LogExec
				@procId = NULL,
				@startPointId_IN = @logExecStartPointId,
				@execStatus = @execStatus,
				@executionTime = @elapsedTime,
				@startPointId_OUT = NULL;

	/* Define novo tempo de procedimento na tabela */
	SET @avgTime = (SELECT AVG(t.tempoExecucao) avgTime
							FROM (
							  SELECT TOP 10 tempoExecucao
							  FROM dbo.ProcLogExec AS ple
							  WHERE ple.tempoExecucao IS NOT NULL AND ple.procId = @procId
							  ORDER BY ple.dataInicio DESC
							) AS t);

	UPDATE dbo.ProcAvailable
			SET tempoMedio = @avgTime,
				ultimaExecucao = GETDATE()
            WHERE procId = @procID;
		
END TRY 
BEGIN CATCH
END CATCH
GO


CREATE OR ALTER PROCEDURE mainprocedures.sp_LogError
	@procID INT,
	@logExecID INT,
	@errNumber INT,
	@errSeverity INT,
	@errState INT,
	@errLine INT,
	@errMessage NVARCHAR(4000)
AS
BEGIN TRY
	INSERT INTO dbo.ProcLogError (procID, logExecID, codigo, severidade, estado, linha, mensagem, criadoEm)
				VALUES (@procID, @logExecID, @errNumber, @errSeverity, @errState, @errLine, @errMessage, GETDATE());
END TRY
BEGIN CATCH
END CATCH
GO


/*Error Map:
	'S' - Sucesso
	'E' - Erro Cr�tico
	'R' - Em Execu��o
	'T' - Demorou mais que o esperado
	'W' - Terminou com Erro
*/
CREATE OR ALTER PROCEDURE mainprocedures.sp_LogExec 
	@procId INT,
	@startPointId_IN INT,
	@execStatus CHAR(1),
	@executionTime INT,
	@startPointId_OUT INT OUTPUT
AS
BEGIN TRY
	DECLARE @IdentityOutput table ( ID int );
	
	IF @startPointId_IN IS NULL BEGIN
			
			INSERT dbo.ProcLogExec (procId, dataInicio, dataFim, [status], tempoExecucao)
				OUTPUT INSERTED.logExecId
						INTO @IdentityOutput
						VALUES (@procID, GETDATE(), NULL, @execStatus, @executionTime);
			
			SET @startPointId_OUT = (select ID from @IdentityOutput);
	END
	ELSE BEGIN
			 UPDATE dbo.ProcLogExec
					SET dataInicio = i.dataInicio, 
						procId = i.procId,
						dataFim = GETDATE(),
						[status] = @execStatus,
						tempoExecucao = @executionTime
					FROM (
						SELECT TOP 1 logExecId, dataInicio, procId 
						FROM  dbo.ProcLogExec WHERE logExecId = @startPointId_IN) AS i
					WHERE 
						dbo.ProcLogExec.logExecId = @startPointId_IN;
		
	END
END TRY 
BEGIN CATCH
END CATCH
GO
/*
----------------------------------------------------------------------
*/