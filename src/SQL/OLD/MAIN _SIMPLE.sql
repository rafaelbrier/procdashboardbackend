USE [Resseguro];
GO

CREATE OR ALTER PROCEDURE [monit].sp_START @procId              INT, 
                                           @logExecStartPointId INT OUTPUT
AS
    BEGIN TRY
        DECLARE @startPointLogExecId INT= NULL; --Identificador do Log de Execu��o do Procedimento

        /* Loga o in�cio do procedimento */

        DECLARE @IdentityOutput TABLE(ID INT);
        INSERT [monit].ProcLogExec
        (procId, 
         dataInicio, 
         dataFim, 
         [status], 
         tempoExecucao
        )
        OUTPUT INSERTED.logExecId
               INTO @IdentityOutput
        VALUES
        (@procID, 
         GETDATE(), 
         NULL, 
         'R', 
         NULL
        );
        SET @logExecStartPointId =
        (
            SELECT ID
            FROM @IdentityOutput
        );
    END TRY
    BEGIN CATCH
    END CATCH;
GO

CREATE OR ALTER PROCEDURE [monit].sp_END @logExecStartPointId INT, 
                                         @errCodigo           NVARCHAR(100)  = NULL, 
                                         @errLine             INT            = NULL, 
                                         @errMessage          NVARCHAR(4000) = NULL
AS
    BEGIN TRY
        DECLARE @execStatus CHAR(1)= 'S';
        DECLARE @elapsedTime INT= NULL;
        DECLARE @procId INT= NULL;
        
        SET @procId =
        (
            SELECT TOP 1 procId
            FROM [monit].ProcLogExec
            WHERE logExecId = @logExecStartPointId
        );

        /* Loga o fim do Procedimento */

        IF(@errCodigo IS NOT NULL
           OR @errLine IS NOT NULL
           OR @errMessage IS NOT NULL)
            BEGIN
                SET @execStatus = 'E';
                INSERT INTO [monit].ProcLogError
                (logExecID, 
                 codigo, 
                 linha, 
                 mensagem, 
                 criadoEm
                )
                VALUES
                (@logExecStartPointId, 
                 @errCodigo, 
                 @errLine, 
                 @errMessage, 
                 GETDATE()
                );
        END;
        UPDATE [monit].ProcLogExec
          SET 
              dataInicio = i.dataInicio, 
              procId = i.procId, 
              dataFim = GETDATE(), 
              [status] = @execStatus, 
              tempoExecucao = DATEDIFF(millisecond, i.dataInicio, GETDATE())
        FROM
        (
            SELECT TOP 1 logExecId, 
                         dataInicio, 
                         procId
            FROM [monit].ProcLogExec
            WHERE logExecId = @logExecStartPointId
        ) AS i
        WHERE [monit].ProcLogExec.logExecId = @logExecStartPointId;
        IF(@procId IS NOT NULL)
            BEGIN
                UPDATE [monit].ProcAvailable
                  SET 
                      ultimaExecucao = GETDATE()
                WHERE procId = @procId;
        END;
    END TRY
    BEGIN CATCH
    END CATCH;
GO

CREATE OR ALTER PROCEDURE [monit].sp_Log_Error @logExecStartPointId INT, 
                                               @errCodigo           NVARCHAR(100)  = NULL, 
                                               @errLine             INT            = NULL, 
                                               @errMessage          NVARCHAR(4000) = NULL
AS
    BEGIN TRY
        INSERT INTO [monit].ProcLogError
        (logExecID, 
         codigo, 
         linha, 
         mensagem, 
         criadoEm
        )
        VALUES
        (@logExecStartPointId, 
         @errCodigo, 
         @errLine, 
         @errMessage, 
         GETDATE()
        );
    END TRY
    BEGIN CATCH
    END CATCH;