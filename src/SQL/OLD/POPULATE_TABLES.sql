USE [master]
GO

	--Table CadastroProcedures
	INSERT INTO dbo.Malha (nome, descricao)
	VALUES ('RPZRESID', 'Malha responsável pela integração de Sinistros SISE/TIA para o BIT/ZRE. Esta malha é composta de (8) Jobs, possui uma periodicidade diária, sendo que entre os dias 01 e 25 os intervalos de execução são de 1 em 1 hora, e do dia 26 até o final do mês esse intervalo é reduzido para 15 minutos para não afetar o fechamento. A janela de operação da malha é de 08h30h. às 23:30h.');
	INSERT INTO dbo.Malha (nome, descricao)
	VALUES ('RPZREDTS', 'Malha responsável pela integração de Prêmios SISE para o BIT/ZRE. Esta malha é composta de 11 Jobs, possui uma periodicidade diária, com intervalos de execução de 1 em 1 hora. A janela de operação da malha é de 06h10h. às 21:10h.');

	INSERT INTO dbo.ProcAvailable (nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio,
		jobName,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		criadoEm
	)
	VALUES('sp_procedure_1',
	   1, 'GAC', 'dbo',
	  'M', 'S',
	  'D', '10:00',
	  'JOBX',
	  'Processo que realiza função de registrar alguma coisa.',
	  '00:00:20', NULL,
	  'Enviar email para o analista responsável caso ocorrar erro ou cancelamento do job. Não haverá plantão.',
		GETDATE()
	 );

	 INSERT INTO dbo.ProcAvailable (nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio,
		jobName,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		criadoEm
	)
	VALUES('sp_procedure_2',
	  1, 'GAC', 'dbo',
	  'M', 'S',
	  'D', '10:00',
	  'JOBY',
	  'Processo que realiza função de registrar alguma coisa.',
	  '00:00:40', NULL,
	  'Enviar email para o analista responsável caso ocorrar erro ou cancelamento do job. Não haverá plantão.',
		GETDATE()
	 );

	 INSERT INTO dbo.ProcAvailable (nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		criadoEm
	)
	VALUES('sp_procedure_3',
	  1, 'GAC', 'dbo',
	  'M', 'S',
	  'D', '10:00',
	  'Processo que realiza função de registrar alguma coisa.',
	  '00:01:00', NULL,
	  'Enviar email para o analista responsável caso ocorrar erro ou cancelamento do job. Não haverá plantão.',
		GETDATE()
	 );

	 INSERT INTO dbo.ProcAvailable (nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		criadoEm
	)
	VALUES('sp_procedure_4',
	  1, 'GAC', 'dbo',
	  'M', 'S',
	  'D', '5:00',
	  'Processo que realiza função de registrar alguma coisa.',
	  '00:01:20', NULL,
	  'Enviar email para o analista responsável caso ocorrar erro ou cancelamento do job. Não haverá plantão.',
		GETDATE()
	 );

	 INSERT INTO dbo.ProcAvailable (nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		criadoEm
	)
	VALUES('sp_procedure_5',
	  1, 'GAC', 'dbo',
	  'M', 'N',
	  'D', '10:30',
	  'Processo que realiza função de registrar alguma coisa.',
	  '00:01:40', NULL,
	  'Enviar email para o analista responsável caso ocorrar erro ou cancelamento do job. Não haverá plantão.',
		GETDATE()
	 );

	 INSERT INTO dbo.ProcAvailable ( nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		criadoEm
	)
	VALUES('sp_procedure_6',
	  2, 'BRZ', 'dbo',
	  'M', 'S',
	  'D', '09:30',
	  'Processo que realiza função de registrar alguma coisa.',
	  '00:02:00', NULL,
	  'Enviar email para o analista responsável caso ocorrar erro ou cancelamento do job. Não haverá plantão.',
		GETDATE()
	 );

	 INSERT INTO dbo.ProcAvailable (nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		criadoEm
	)
	VALUES('sp_procedure_7',
	  2, 'BRZ', 'dbo',
	  'M', 'N',
	  'D', '09:30',
	  'Processo que realiza função de registrar alguma coisa.',
	  '00:02:20', NULL,
	  'Enviar email para o analista responsável caso ocorrar erro ou cancelamento do job. Não haverá plantão.',
		GETDATE()
	 );

	 INSERT INTO dbo.ProcAvailable (nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		criadoEm
	)
	VALUES('sp_procedure_8',
	  2, 'BRZ', 'dbo',
	  'M', 'N',
	  'D', '09:30',
	  'Processo que realiza função de registrar alguma coisa.',
	  '00:02:40', NULL,
	  'Enviar email para o analista responsável caso ocorrar erro ou cancelamento do job. Não haverá plantão.',
		GETDATE()
	 );

	 INSERT INTO dbo.ProcAvailable ( nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		criadoEm
	)
	VALUES( 'sp_procedure_9',
	  2, 'BRZ', 'dbo',
	  'M', 'N',
	  'D', '09:30',
	  'Processo que realiza função de registrar alguma coisa.',
	  '00:03:00', NULL,
	  'Enviar email para o analista responsável caso ocorrar erro ou cancelamento do job. Não haverá plantão.',
		GETDATE()
	 );

	 INSERT INTO dbo.ProcAvailable (
		nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		criadoEm
	)
	VALUES('sp_procedure_10',
	  2, 'BRZ', 'dbo',
	  'M', 'N',
	  'D', '09:30',
	  'Processo que realiza função de registrar alguma coisa.',
	  '00:03:20', NULL,
	  'Enviar email para o analista responsável caso ocorrar erro ou cancelamento do job. Não haverá plantão.',
		GETDATE()
	 );

	 INSERT INTO dbo.ProcAvailable (nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, diaInicio,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		criadoEm
	)
	VALUES('sp_procedure_11',
	  2, 'BRZ', 'dbo',
	  'M', 'N',
	  'S', '09:30', '2',
	  'Processo que realiza função de registrar alguma coisa.',
	  '00:02:20', NULL,
	  'Enviar email para o analista responsável caso ocorrar erro ou cancelamento do job. Não haverá plantão.',
		GETDATE()
	 );

	  INSERT INTO dbo.ProcAvailable (nome,
		malhaId, sistema, banco,
		criticidade, fechamento,
		periodicidade, horaInicio, diaInicio,
		descricao,
		tempoEstimado, ultimaExecucao,
		observacao,
		criadoEm
	)
	VALUES('sp_procedure_12',
	  2, 'BRZ', 'dbo',
	  'M', 'N',
	  'M', '11:30', '2',
	  'Processo que realiza função de registrar alguma coisa.',
	  '00:02:20', NULL,
	  'Enviar email para o analista responsável caso ocorrar erro ou cancelamento do job. Não haverá plantão.',
		GETDATE()
	 );
	 				
GO	



