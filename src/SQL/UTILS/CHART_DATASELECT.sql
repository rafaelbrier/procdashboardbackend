SELECT AVG(tempoExecucao) AS tempoExecucao,
	   CAST(p.dataInicio AS DATE) as dataInicio,
	   SUM((CASE WHEN p.STATUS = 'E'
			    THEN 1
			    ELSE 0
	       END)) AS errorNumero,
	  COUNT(p.logExecId) AS numeroExecucao
 FROM dbo.ProcLogExec p
 WHERE p.procId = 5
 GROUP BY CAST(p.dataInicio AS DATE)

 SELECT COUNT(*) FROM dbo.ProcLogExec ple WHERE ple.procId = 5 AND ple.dataInicio >= GETDATE() - 2