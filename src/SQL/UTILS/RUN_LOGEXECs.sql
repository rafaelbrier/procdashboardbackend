/****** Script for SelectTopNRows command from SSMS  ******/
USE [Resseguro]
GO
BEGIN
	DECLARE @dateTime DATETIME;
	DECLARE @dateEnd DATETIME;

	DECLARE @i INT;
	SET @i = 1;
	SET @dateTime = GETDATE() - 120; -- 3 meses atrás
	
	WHILE @i < 90 BEGIN
	SET @dateEnd = @dateTime + @i;
	SET @dateEnd = DATEADD(ss, 100, @dateEnd);
	INSERT INTO [monit].LOG_EXEC(dataFim, dataInicio, procId,  status,  tempoExecucao)
	VALUES
	(
	    @dateEnd , -- dataFim - datetime2
	    @dateTime + @i, -- dataInicio - datetime2
	    6, -- procId - int
	    'S', -- status - varchar
	    (50+@i)*1000 -- tempoExecucao - int
	)
	SET @i = @i + 1;
	END
 END