/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [logErrorId]
      ,[codigo]
      ,[criadoEm]
      ,[linha]
      ,[logExecId]
      ,[mensagem]
  FROM [DB_Testes].[dbo].[ProcLogError]

INSERT INTO dbo.ProcLogError(codigo,criadoEm,linha,logExecId, mensagem)
VALUES('5000', -- codigo - varchar
    GETDATE(), -- criadoEm - datetime2
    23, -- linha - int
    29, -- logExecId - int
    'TESTANDO' -- mensagem - text
)